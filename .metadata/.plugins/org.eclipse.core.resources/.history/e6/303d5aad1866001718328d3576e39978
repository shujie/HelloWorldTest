/**************************************************************************************************
 * @file   : StRecognition.h
 * @brief  : 图片人脸识别
 * @date   : 2017-05-08
 * @remarks:
 **************************************************************************************************/

#ifndef STRECOGNITION_H
#define STRECOGNITION_H

#include <string>
#include <opencv2/opencv.hpp>
#include<vector>
#include "SACoreDef.h"
#include "StComDef.h"
#include "StIndexTrain.h"
using namespace std;
class StGpuIndex;
class StSimpleExtractor;

struct ScoreNode
{
    long id;
    std::string name;
    float dist;
    float realDist;

    ScoreNode(long id, std::string name, float dist, float realDist)
        :id(id), name(name), dist(dist), realDist(realDist)
    {}

    bool operator < (const ScoreNode &other) const
    {
        return realDist < other.realDist;
    }

    bool operator > (const ScoreNode &other) const
    {
        return realDist > other.realDist;
    }

    bool operator <=(const ScoreNode &other) const
    {
        return realDist <= other.realDist;
    }
    bool operator >=(const ScoreNode &other) const
    {
        return realDist >= other.realDist;
    }
};

struct SearchResult
{
    int top1;
    int top10;
    int top50;
    int total;

    SearchResult()
        :top1(0), top10(0), top50(0), total(0)
    {}
};

class StRecognition
{
public:
    StRecognition(int deviceID, const IndexConfig &indexConf);
    StRecognition(const IndexConfig &indexConf, ExtractConf *conf, int probe = 1);
    virtual ~StRecognition();

    void setFactor(int factor);

    void search(const std::string &imageName, int topK = 10);

    void search(FeatureValue features, int topK , vector<ScoreNode> vtNodes);
    void searchTest(vector<float> features);
    vector<float> getIndexFeature(vector<float> features, size_t start, size_t end);

    void showResult(int top, const std::string &target, float dist, float realDist);

    //自动测试一个文件夹中的人脸
    void searchTest(const std::string &dirPath, int topk);

    void searchTestByShu(vector<FeatureValue>features, int topk);
    void testOneByShu(vector<float> feature, int topk);
private:
    //计算相似度
    float computerDistance(const FeatureValue &ft1, float *ft2);

    void getImages(const std::string &dirPath, std::vector<std::string> &images);

    void testOne(const std::string &image, int topk, SearchResult &result);

    //判断是否是同一人
    bool isSame(const std::string &fileName1, const std::string &fileName2);
private:
    StGpuIndex *mIndex;
    StSimpleExtractor *mExtractor;

    IndexType mType;
    //搜索外扩范围系数
    int mFactor;
};




#endif // STRECOGNITION

