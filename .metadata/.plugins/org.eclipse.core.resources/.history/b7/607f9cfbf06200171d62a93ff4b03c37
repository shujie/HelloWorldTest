/**************************************************************************************************
 * @file   : StInDB.cpp
 * @brief  : 入库示例
 * @date   : 2017-05-08
 * @remarks:
 **************************************************************************************************/
#include <fstream>
#include <glog/logging.h>
#include "StDistributor.h"
#include "StInDB.h"
#include "StMongoDB.h"
#include "StGpuIndex.h"

using namespace std;




StInDB::StInDB() : mDistributor(NULL), mConf(NULL), mFeatureCount(0), mSimCount(0)
{
    mStatus = new StatusInfo;
}

StInDB::~StInDB()
{
    if(mDistributor)
        delete mDistributor;

    delete mStatus;
}

//初始化索引
bool StInDB::initIndex(int deviceID, const IndexConfig &info)
{
    mIndexConfig = info;
    bool ret = true;
    mIndex = new StGpuIndex(info.type);
    if(info.type == FlatIP)
    {
        ret = mIndex->open(deviceID, info.dim);
    }
    else
    {
        ret = mIndex->open(deviceID, info.srcFile);//？？？？？？

    }
    return ret;
}

//设置特征提取的参数
void StInDB::setExtractArg(const std::vector<std::string> &dataDirs, ExtractConf *extConf, int featureDim)
{
    mConf = extConf;
    mFeatureDim = featureDim;
    mDistributor = new StFileDistributor(extConf, dataDirs);
    mDistributor->setDBInterface(this);//
    mDistributor->setMonitor(this);
}

//重写的特征数据接口
void StInDB::outFeatures(VerifyInfo *vfInfo, cv_faceFeature_t *features)
{
    //建立索引
    vector<long> ids;
    mIndex->addToIndex(vfInfo->mats.size(), features, ids);
    //插入到数据库
    MongoDBPtr->insertBatch(ids.data(), vfInfo->names, features);
}

//事件处理
void StInDB::event(QmEvent *aEvent)
{
    switch(aEvent->type())
    {
    case QmEvent::ET_User + 1 :
        {
            StDetectEvent *pDetectEvent = dynamic_cast<StDetectEvent *>(aEvent);
            vector<StatusUnit> vtStatus;
            pDetectEvent->getStatus(vtStatus);
            for(size_t i = 0; i< vtStatus.size(); i++)
            {
                statisticStatus(vtStatus[i]);
            }
        }
        break;
    case QmEvent::ET_User + 2 :
        {
            StExtractEvent *pExtEvent = dynamic_cast<StExtractEvent *>(aEvent);
            switch(pExtEvent->status())
            {
            case SC_VerifyError:
                mStatus->verifyError += pExtEvent->imageCount();
                break;
            case SC_CompleteExtract:
                mStatus->imageCount += pExtEvent->imageCount();
                break;
            default:
                break;
            }
        }
        break;
    default:
        break;
    }
    showStatus();
}

//开始训练
void StInDB::start()
{
    if(!mDistributor)
    {
        LOG(ERROR) << "feature extract arg not set!" << endl;
        return;
    }

    //开启分发
    TS(ext);
    mDistributor->start();
    eventLoop();
    printf("\r\n");

    //模拟数据,并插入了数据库
    simulate();
    printf("\r\n");
    mIndex->saveIndex(mIndexConfig.dstFile);
    TE(ext, "extract time: ");
}

//统计状态
void StInDB::statisticStatus(const StatusUnit &unit)
{
    switch(unit.status)
    {
    case SC_ReadImageError:
        mStatus->readImageError++;
        break;
    case SC_DetectError:
        mStatus->detectError++;
        break;
    case SC_NoFace:
        mStatus->noFace++;
        break;
    case SC_AlignError:
        mStatus->alignError++;
        break;
    case SC_BlackAndWhite:
        mStatus->filterBlackAndWhite++;
        break;
    case SC_QualityLimit:
        mStatus->qualityLimit++;
        break;
    case SC_FaceSizeLimit:
        mStatus->faceSizeLimit++;
        break;
    case SC_VerifyError:
        mStatus->verifyError++;
        break;
    default:
        break;
    }
}

//显示进度状态
void StInDB::showStatus()
{
    printf("Success image:%d, Read Error:%d, Detect Error:%d, NoFace:%d, AlignError:%d, FilterBlackAndWhite:%d, QualityLimit:%d, FaceSizeLimit:%d, VerifyError:%d\r",
           mStatus->imageCount, mStatus->readImageError, mStatus->detectError, mStatus->noFace, mStatus->alignError, mStatus->filterBlackAndWhite,
           mStatus->qualityLimit, mStatus->faceSizeLimit, mStatus->verifyError);
    fflush(stdout);
}

//设置模拟源数据文件
void StInDB::setDataFile(const std::string &dataFile, int simCount)
{
    mDataFile = dataFile;
    mSimCount = simCount;
}

void StInDB::simulate()
{
    if(mSimCount <= 0)
        return;
    ifstream in(mDataFile.c_str(), std::ios::binary);
    for(int i = 0; i < mSimCount; i++)
    {
        simulateOne(in);
    }
    in.close();
}



void StInDB::simulateOne(std::ifstream &in)
{
    int ftCount = 200000;
    int batch = 100;
    int m = 0;
    vector<float> vtData(batch * mIndexConfig.dim);
    vector<long> ids;
    vector<string> vtNames(batch);
    for(int i = 0; i < ftCount; i++)
    {
        float * buf = vtData.data() + m * mFeatureDim;
        in.read((char *)buf, sizeof(float) * mIndexConfig.dim);
        forgeFeature(buf, mFeatureDim);
        vtNames[m] = "simulate datas";
        if((m + 1) % batch == 0)
        {
            mIndex->addToIndex(batch, vtData, ids);
            MongoDBPtr->insertBatch(ids.data(), vtNames, vtData, mIndexConfig.dim);
            mStatus->imageCount += batch;
            printf("simulate features : %d\r", mStatus->imageCount);
            fflush(stdout);
            m = 0;
        }
        else
            m++;
    }
    in.seekg(0);
}

//DB中插入真实的数据
void StInDB::insertTFeatures(vector<float>& features)
{
	size_t ftCount = features.size();
	int batch = 100;
	int m = 0;
	vector<float> vtData(batch * 256);
	vector<long> ids;
	vector < string > vtNames(batch,"shujie");
	/*size_t current = 0;
	for (size_t i = 0; i < ftCount/(256 * 100) ; i++)
	{
		//vtData.insert(vtData.begin() + m*256, features.begin() + i * 256, features.begin()+(i+1)*256 - 1 );

		for(size_t j = 0; j < (size_t)100; ++j)
		{
			for(size_t k = 0; k < 256; ++k)
			{
				vtData[j*256 + k] = features[i*100*256 + j*256 + k];
			}
		}*/
		//vtNames[m] = "simulate datas";
		//if ((m + 1) % batch == 0)
		//{
			mIndex->addToIndex(batch, vtData, ids);
			MongoDBPtr->insertBatch(ids.data(), vtNames, features, 256);
			fflush (stdout);
			//m = 0;

		//} else
			//m++;
	}
	std::cout << "!!!!      ...... 开始保存索引....     !!!!" << std::endl;
	std::cout << "当前索引的总数： "<< mIndex->indexCount()<<endl;
	mIndex->saveIndex("./240FlatIP_shu.idx");
	std::cout << "!!!!      ......保存索引成功....     !!!!" << std::endl;
}
//伪造特征数据
void StInDB::forgeFeature(float *ft, int len)
{
    double dSum = 0.0;
    for(int i = 0; i < len; i++)
    {
        ft[i] = drand48();//(float)(rand() % 100) / 100.0;
        dSum += ft[i] * ft[i];
    }
    dSum = sqrt(dSum);
    for(int i = 0; i < len; i++)
    {
        ft[i] = ft[i] / dSum;
    }
}
