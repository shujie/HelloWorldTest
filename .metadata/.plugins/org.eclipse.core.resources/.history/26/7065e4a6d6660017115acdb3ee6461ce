/**************************************************************************************************
 * @file   : StRecognition.cpp
 * @brief  : 图片人脸识别
 * @date   : 2017-05-08
 * @remarks:
 **************************************************************************************************/

#include <iostream>
#include <sys/stat.h>
#include <sys/dir.h>
#include <QmFile.h>
#include <vector>
#include<sys/time.h>
#include<unistd.h>
#include "StGpuIndex.h"
#include "StSimpleExtractor.h"
#include "StRecognition.h"
#include "StMongoDB.h"

using namespace std;
using namespace cv;

StRecognition::StRecognition(int deviceID, const IndexConfig &indexConf) : mExtractor(NULL), mFactor(1)
{
    mIndex = new StGpuIndex(indexConf.type);
    mIndex->open(deviceID, indexConf.dstFile);

}

StRecognition::StRecognition(const IndexConfig &indexConf, ExtractConf *conf, int probe) : mFactor(1)
{
   /*mIndex = new StGpuIndex(indexConf.type);
    mIndex->open(conf->gpuID, indexConf.dstFile);
    mIndex->setProbes(probe);

    mExtractor = new StSimpleExtractor(conf);
    mExtractor->init();
   ;*/
	mIndex = new StGpuIndex(IVFPQ);
	mIndex->open(0 ,"./240IVFPQ.idx");
	mIndex->setProbes(32);

	/// read target， alarm and disturb features from file
	StIndexTrain indexTrain;
	indexTrain.mFeatureCount = 256;
	std::vector<float> disturb_features;
	finalResult = 0;
	timeUsed = 0.0;

	indexTrain.readFeatureFromText("feature_target.txt", target_features,
			"feature_source.txt", source_features, indexMap, 256);
	indexTrain.readFeatureFromFile("./1kw/1kw.feature", disturb_features, 256);

	int target_size = target_features.size() / 256;
	int source_size = source_features.size() / 256;
	int disturb_size = disturb_features.size() / 256;

	targets.resize(target_features.size()/256);//28

	for (size_t i = 0; i < target_features.size() / 256; ++i) {
			for (size_t j = 0; j < 256; ++j) {
				targets[i].push_back(target_features[i * 256 + j]);
			}
		}

	/// merge disturb and target features into one vector,modify this line
	/*disturb_features.insert(disturb_features.end(), target_features.begin(),
			target_features.end());*/
	target_features.insert(target_features.end(), disturb_features.begin(), disturb_features.end());
	int merge_size = target_size + disturb_size;


	/// add features into mIndex and MongoDB
	vector<long> ids;
	mIndex->addToIndex(merge_size, target_features, ids);

	vector<string> vtNames(merge_size, "shujie");
	MongoDBPtr->insertBatch(ids.data(), vtNames, target_features, 256);
}

StRecognition::~StRecognition()
{
    delete mIndex;
    if(mExtractor)
        delete mExtractor;
}

void StRecognition::setFactor(int factor)
{
    mFactor = factor;
}

void StRecognition::search(const std::string &imageName, int topK)
{
    Mat mat = imread(imageName);
    if(!mat.data)
    {
        cout << "read image failed!" << endl;
        return;
    }
    vector<cv_alignment_t> alignInfos;
    vector<FeatureValue> features;
    if(!mExtractor->extract(mat, alignInfos, features))
        return;

    if(features.size() == 0)
        return;
    if(features.size() > 1)
    {
        cout << "has two face, use the first one" << endl;
    }
    int nK = mType == IVFPQ ? topK * mFactor : topK;
    vector<long> idxs;
    vector<float> dist;
    mIndex->search(features[0], nK, idxs, dist);

    if(idxs.size() == 0)
        return;

    vector<ScoreNode> vtNodes;
    for(int i = 0; i < nK; i++)
    {
        if(idxs[i] <= 0)
            continue;
        string strName;
        FeatureValue ftValue;
        if(!MongoDBPtr->search(idxs[i], strName, ftValue))
        {
            cout << "search feature datebase failed!" << endl;
            continue;
        }
        float realDist = computerDistance(features[0], ftValue.data());
        if(mType == FlatIP)
            //showResult(i + 1, strName, dist[i], realDist);
        	cout<<"FlatIP"<<endl;
        else
        {
            vtNodes.push_back(ScoreNode(idxs[i], strName, dist[i], realDist));
        }
    }
    if(mType == IVFPQ)
    {
        std::sort(vtNodes.begin(), vtNodes.end(), greater<ScoreNode>());
        for(size_t i = 0; i < vtNodes.size(); i++)
        {
            if(i >= topK)
                break;
          //  showResult(i + 1, vtNodes[i].name, vtNodes[i].dist, vtNodes[i].realDist);
            cout<<" 	IVFPQ 	"<<std::endl;
        }
    }
}

/*void StRecognition::showResult()
{
    cout <<"result: "<<result<<"/266 = "<<result/266<<std::endl;
}*/

void StRecognition::showResult(int top, const std::string &target, float dist, float realDist)
{
	std::cout<<top;
}
//计算相似度
float StRecognition::computerDistance(const FeatureValue &ft1, float *ft2)
{
    int dim = ft1.size();
    float scores = 0;
    for(int i = 0; i < dim; i++)
    {
        scores += ft1[i] * ft2[i];
    }
    return scores;
}

//自动测试一个文件夹中的人脸
void StRecognition::searchTest(const std::string &dirPath, int topk)
{
    SearchResult result;
    vector<string> vtImages;
    getImages(dirPath, vtImages);
    for(size_t i = 0; i < vtImages.size(); i++)
    {
        testOne(vtImages[i], topk, result);
        result.total++;
    }
    if(result.total == 0)
        return;
    cout << "top 1 : " << result.top1 << " / " << result.total << " = " << (float)result.top1 / (float)result.total << endl;
    cout << "top 10 : " << result.top10 << " / " << result.total << " = " << (float)result.top10 / (float)result.total << endl;
    cout << "top 50 : " << result.top50 << " / " << result.total << " = " << (float)result.top50 / (float)result.total << endl;
}

void StRecognition::getImages(const std::string &dirPath, std::vector<std::string> &images)
{
    DIR *dir = opendir(dirPath.c_str());
    if(!dir)
        return;
    dirent *file;
    struct stat statbuf;
    string fileName;

    while((file = readdir(dir)) != 0)
    {
        if(strncmp(file->d_name, ".", 1) == 0)
            continue;
        string str = dirPath + "/" + string(file->d_name);
        lstat(str.c_str(), &statbuf);
        if(S_ISDIR(statbuf.st_mode))
        {
            continue;
        }
        //检查是否是jpg图片
        fileName = string(file->d_name);
        int index = fileName.find_last_of('.');
        string strType = fileName.substr(index + 1, fileName.length() - index - 1);
        std::transform(strType.begin(), strType.end(), strType.begin(), ::tolower);
        if(strType.compare("jpg") == 0 || strType.compare("png") == 0 || strType.compare("bmp") == 0)
        {
            string strAll = dirPath + "/" + fileName;
            images.push_back(strAll);
        }
    }
    closedir(dir);
}

void StRecognition::testOne(const std::string &image, int topk, SearchResult &result)
{
    Mat mat = imread(image);
    if(!mat.data)
    {
        cout << "read image failed!" << endl;
        return;
    }
    vector<cv_alignment_t> alignInfos;
    vector<FeatureValue> features;
    if(!mExtractor->extract(mat, alignInfos, features))
        return;

    if(features.size() == 0)
        return;
    if(features.size() > 1)
    {
        cout << "has two face, use the first one" << endl;
    }
    int nK = topk * mFactor;
    vector<long> idxs;
    vector<float> dist;
    mIndex->search(features[0], nK, idxs, dist);

    if(idxs.size() == 0)
        return;

    vector<ScoreNode> vtNodes;
    for(int i = 0; i < nK; i++)
    {
        if(idxs[i] <= 0)
            continue;
        string strName;
        FeatureValue ftValue;
        if(!MongoDBPtr->search(idxs[i], strName, ftValue))
        {
            cout << "search feature datebase failed!" << endl;
            continue;
        }
        float realDist = computerDistance(features[0], ftValue.data());
        vtNodes.push_back(ScoreNode(idxs[i], strName, dist[i], realDist));
    }

    std::sort(vtNodes.begin(), vtNodes.end(), greater<ScoreNode>());
    for(size_t i = 0; i < vtNodes.size(); i++)
    {
        if(i >= topk)
            break;
        //showResult(i + 1, vtNodes[i].name, vtNodes[i].dist, vtNodes[i].realDist);
        cout<<"FUCK TEST ONE"<<endl;
        if(isSame(image, vtNodes[i].name))
        {
            if(i == 0)
                result.top1++;
            if(i < 10)
                result.top10++;
            if(i < 50)
                result.top50++;
        }
    }
}

//判断是否是同一人
bool StRecognition::isSame(const std::string &fileName1, const std::string &fileName2)
{
    QmFile file1(fileName1);
    string strName1 = file1.baseName();
    string strName2 = QmFile(fileName2).baseName();

    if(strName2.substr(0, 4).compare("STTT") != 0)
        return false;

    string strStart1 = strName1.substr(0, 7);
    string strStart2 = strName2.substr(0, 7);
    if(strStart1.compare(strStart2) == 0)
        return true;
    else
        return false;
}


/*bool StRecognition::is_equal(vector<float>& source, vector<float>& target)
{
	if(source.size() != target.size())
		return false;

	bool flag = true;
	for(size_t i=0; i<source.size(); ++i)
	{
		if(source[i] != target[i])
			flag = false;
	}
	return flag;
}*/
void StRecognition::searchTestByShu(vector<FeatureValue>features, int topk)
{

    vector<string> vtImages;

    for(size_t i = 0; i < features.size(); i++)
    {
    	testOneByShu(features[i], topk, i);
    }

    cout<<" Time="<<(timeUsed/1000)/266<<" ms"<<endl;
    std::cout<<"~~~~~~!!!!!!!!! finalResult/266="<<(float)finalResult/2.66<<"%   !!!!!!!!!!!!!!!!!~~~~~~~~~~~~~~~~"<<std::endl;
}

void StRecognition::testOneByShu(vector<float> feature, int topk ,int index) {
	int nK = topk * mFactor;	//mFactor 默认是1
	vector<long> idxs;
	vector<float> dist;
	/*特征入库与中心点做了一个搜索，找到最近的中心点***/
	struct timeval tstart, tend;
	struct timeval tstart_db, tend_db;

	gettimeofday(&tstart, NULL);
	mIndex->search(feature, nK, idxs, dist);
	gettimeofday(&tend,NULL);
	vector<ScoreNode> vtNodes;

	if(idxs.size() == 0)
	        return;

	for (int i = 0; i < nK; i++) {
		string strName;
		FeatureValue ftValue;
		gettimeofday(&tstart_db, NULL);
		if (!MongoDBPtr->search(idxs[i], strName, ftValue))
		{
			cout << "search feature datebase failed!" << endl;
			continue;
		}
		gettimeofday(&tend_db, NULL);

		float realDist = computerDistance(feature, ftValue.data());
		vtNodes.push_back(ScoreNode(idxs[i], strName, dist[i], realDist));
	}

	timeUsed += 1000000*(tend.tv_sec-tstart.tv_sec)+tend.tv_usec-tstart.tv_usec + 1000000*(tend_db.tv_sec-tstart_db.tv_sec)+tend_db.tv_usec-tstart_db.tv_usec;
	std::sort(vtNodes.begin(), vtNodes.end(), greater<ScoreNode>());

	size_t bestID = vtNodes[0].id;
	//insert after disturb features 10000000
	size_t indexID = indexMap[index];
	if (indexID == bestID) {
		finalResult++;
	}

}



/***将栋梁传过来的特征向量转化为子向量，其中start表示取的第一个位置，end表示相应的最后一个位置，位置从0开始***/

vector<float> StRecognition::getIndexFeature(vector<float> features, size_t start, size_t end)
{
	if(start>features.size() || end >features.size())
	{
		cout<<"out of range.......!!";
		return vector<float>();
	}
	else{
		return vector<float>(features.begin()+start,features.begin()+end);
	}
}

/**************************************************
 * @参数:*******************************************
 * features:传入的每一个人脸的特征向量******************
 * topK:对于每一个特征最匹配的前K数量*******************
 * vtNodes:输出参数，排好序的特征信息的ScoreNode集合*****
 * 该函数主要功能是：将传入的特征与数据库中信息做比对，并****
 * 将比对信息存储与vtNodes里面************************
 **************************************************/

void StRecognition::search( FeatureValue features, int topK, vector<ScoreNode> vtNodes)
{
	int nK = mType == IVFPQ ? topK * mFactor : topK;
	vector<long> idxs;
	vector<float> dist;
	for(size_t j=0; j<features.size(); ++j)
	{
		std::cout<<features[j]<<" ";
	}
	mIndex->search(features, nK, idxs, dist);
	if (idxs.size() == 0)
	{
		return;
	}
	for (int i = 0; i < nK; i++) {
		if (idxs[i] <= 0)
			continue;
		string strName;
		FeatureValue ftValue;
		if (!MongoDBPtr->search(idxs[i], strName, ftValue))
		{
			cout << "search feature datebase failed!" << endl;
			continue;
		}
		float realDist = computerDistance(features, ftValue.data());
		if (mType == FlatIP)
		//	showResult(i + 1, strName, dist[i], realDist);
			cout<<"FUCK YOU"<<endl;
		else {
			vtNodes.push_back(ScoreNode(idxs[i], strName, dist[i], realDist));
		}
	}
	if (mType == IVFPQ)
		std::sort(vtNodes.begin(), vtNodes.end(), greater<ScoreNode>());
}

/**************************************************
 * @参数:*******************************************
 * features:传入的特征向量集合（每一个特征占据256维）*****
 * 该函数主要是将：1,传入的特征向量集合拆分成每一个256维****
 * 的子向量，     2,将每一个特征都传入数据库中去做搜索，****
 * 并将搜索的结果存储在向量中。*************************
 **************************************************/

void StRecognition::searchTest(vector<float> features)
{
	if( features.size() == (size_t)0 )
	{
		cout<<" the number of the features is 0.......!";
		return;
	}
	size_t numOfFeatures = features.size()/256;
	vector< vector<ScoreNode> >vtNodes(numOfFeatures,vector<ScoreNode>());
	for(size_t i=0; i<numOfFeatures; ++i)
	{
		vector<float> feature = getIndexFeature(features, i*256, (i+1)*256-1);
		search(feature, 10, vtNodes[i]);
	}

}
