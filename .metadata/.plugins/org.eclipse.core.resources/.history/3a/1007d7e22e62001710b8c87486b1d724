/**************************************************************************************************
 * @file   : StRecognition.cpp
 * @brief  : 图片人脸识别
 * @date   : 2017-05-08
 * @remarks:
 **************************************************************************************************/

#include <iostream>
#include <sys/stat.h>
#include <sys/dir.h>
#include <QmFile.h>
#include <vector>
#include "StGpuIndex.h"
#include "StSimpleExtractor.h"
#include "StRecognition.h"
#include "StMongoDB.h"

using namespace std;
using namespace cv;

StRecognition::StRecognition(int deviceID, const IndexConfig &indexConf) : mExtractor(NULL), mFactor(1)
{
    mIndex = new StGpuIndex(indexConf.type);
    mIndex->open(deviceID, indexConf.dstFile);
}

StRecognition::StRecognition(const IndexConfig &indexConf, ExtractConf *conf, int probe) : mFactor(1)
{
    mIndex = new StGpuIndex(indexConf.type);
    mIndex->open(conf->gpuID, indexConf.dstFile);
    mIndex->setProbes(probe);

    mExtractor = new StSimpleExtractor(conf);
    mExtractor->init();
    mType = indexConf.type;
}

StRecognition::~StRecognition()
{
    delete mIndex;
    if(mExtractor)
        delete mExtractor;
}

void StRecognition::setFactor(int factor)
{
    mFactor = factor;
}

void StRecognition::search(const std::string &imageName, int topK)
{
    Mat mat = imread(imageName);
    if(!mat.data)
    {
        cout << "read image failed!" << endl;
        return;
    }
    vector<cv_alignment_t> alignInfos;
    vector<FeatureValue> features;
    if(!mExtractor->extract(mat, alignInfos, features))
        return;

    if(features.size() == 0)
        return;
    if(features.size() > 1)
    {
        cout << "has two face, use the first one" << endl;
    }
    int nK = mType == IVFPQ ? topK * mFactor : topK;
    vector<long> idxs;
    vector<float> dist;
    mIndex->search(features[0], nK, idxs, dist);

    if(idxs.size() == 0)
        return;

    vector<ScoreNode> vtNodes;
    for(int i = 0; i < nK; i++)
    {
        if(idxs[i] <= 0)
            continue;
        string strName;
        FeatureValue ftValue;
        if(!MongoDBPtr->search(idxs[i], strName, ftValue))
        {
            cout << "search feature datebase failed!" << endl;
            continue;
        }
        float realDist = computerDistance(features[0], ftValue.data());
        if(mType == FlatIP)
            showResult(i + 1, strName, dist[i], realDist);
        else
        {
            vtNodes.push_back(ScoreNode(idxs[i], strName, dist[i], realDist));
        }
    }
    if(mType == IVFPQ)
    {
        std::sort(vtNodes.begin(), vtNodes.end(), greater<ScoreNode>());
        for(size_t i = 0; i < vtNodes.size(); i++)
        {
            if(i >= topK)
                break;
            showResult(i + 1, vtNodes[i].name, vtNodes[i].dist, vtNodes[i].realDist);
        }
    }
}

void StRecognition::showResult(int top, const std::string &target, float dist, float realDist)
{
    cout <<  top << " : distance = " << dist << ", realDist = " << realDist << ", file:" << target << endl ;
}

//计算相似度
float StRecognition::computerDistance(const FeatureValue &ft1, float *ft2)
{
    int dim = ft1.size();
    float scores = 0;
    for(int i = 0; i < dim; i++)
    {
        scores += ft1[i] * ft2[i];
    }
    return scores;
}

//自动测试一个文件夹中的人脸
void StRecognition::searchTest(const std::string &dirPath, int topk)
{
    SearchResult result;
    vector<string> vtImages;
    getImages(dirPath, vtImages);
    for(size_t i = 0; i < vtImages.size(); i++)
    {
        testOne(vtImages[i], topk, result);
        result.total++;
    }
    if(result.total == 0)
        return;
    cout << "top 1 : " << result.top1 << " / " << result.total << " = " << (float)result.top1 / (float)result.total << endl;
    cout << "top 10 : " << result.top10 << " / " << result.total << " = " << (float)result.top10 / (float)result.total << endl;
    cout << "top 50 : " << result.top50 << " / " << result.total << " = " << (float)result.top50 / (float)result.total << endl;
}

void StRecognition::getImages(const std::string &dirPath, std::vector<std::string> &images)
{
    DIR *dir = opendir(dirPath.c_str());
    if(!dir)
        return;
    dirent *file;
    struct stat statbuf;
    string fileName;

    while((file = readdir(dir)) != 0)
    {
        if(strncmp(file->d_name, ".", 1) == 0)
            continue;
        string str = dirPath + "/" + string(file->d_name);
        lstat(str.c_str(), &statbuf);
        if(S_ISDIR(statbuf.st_mode))
        {
            continue;
        }
        //检查是否是jpg图片
        fileName = string(file->d_name);
        int index = fileName.find_last_of('.');
        string strType = fileName.substr(index + 1, fileName.length() - index - 1);
        std::transform(strType.begin(), strType.end(), strType.begin(), ::tolower);
        if(strType.compare("jpg") == 0 || strType.compare("png") == 0 || strType.compare("bmp") == 0)
        {
            string strAll = dirPath + "/" + fileName;
            images.push_back(strAll);
        }
    }
    closedir(dir);
}

void StRecognition::testOne(const std::string &image, int topk, SearchResult &result)
{
    Mat mat = imread(image);
    if(!mat.data)
    {
        cout << "read image failed!" << endl;
        return;
    }
    vector<cv_alignment_t> alignInfos;
    vector<FeatureValue> features;
    if(!mExtractor->extract(mat, alignInfos, features))
        return;

    if(features.size() == 0)
        return;
    if(features.size() > 1)
    {
        cout << "has two face, use the first one" << endl;
    }
    int nK = topk * mFactor;
    vector<long> idxs;
    vector<float> dist;
    mIndex->search(features[0], nK, idxs, dist);

    if(idxs.size() == 0)
        return;

    vector<ScoreNode> vtNodes;
    for(int i = 0; i < nK; i++)
    {
        if(idxs[i] <= 0)
            continue;
        string strName;
        FeatureValue ftValue;
        if(!MongoDBPtr->search(idxs[i], strName, ftValue))
        {
            cout << "search feature datebase failed!" << endl;
            continue;
        }
        float realDist = computerDistance(features[0], ftValue.data());
        vtNodes.push_back(ScoreNode(idxs[i], strName, dist[i], realDist));
    }

    std::sort(vtNodes.begin(), vtNodes.end(), greater<ScoreNode>());
    for(size_t i = 0; i < vtNodes.size(); i++)
    {
        if(i >= topk)
            break;
        showResult(i + 1, vtNodes[i].name, vtNodes[i].dist, vtNodes[i].realDist);
        if(isSame(image, vtNodes[i].name))
        {
            if(i == 0)
                result.top1++;
            if(i < 10)
                result.top10++;
            if(i < 50)
                result.top50++;
        }
    }
}

//判断是否是同一人
bool StRecognition::isSame(const std::string &fileName1, const std::string &fileName2)
{
    QmFile file1(fileName1);
    string strName1 = file1.baseName();
    string strName2 = QmFile(fileName2).baseName();

    if(strName2.substr(0, 4).compare("STTT") != 0)
        return false;

    string strStart1 = strName1.substr(0, 7);
    string strStart2 = strName2.substr(0, 7);
    if(strStart1.compare(strStart2) == 0)
        return true;
    else
        return false;
}

void StRecognition::searchTestByShu(vector<FeatureValue>features, int topk)
{
    SearchResult result;
    vector<string> vtImages;
   // getImages(dirPath, vtImages);
    for(size_t i = 0; i < features.size(); i++)
    {
        testOneByShu(features[i], topk, result);
        result.total++;
    }
    if(result.total == 0)
        return;
    cout << "top 1 : " << result.top1 << " / " << result.total << " = " << (float)result.top1 / (float)result.total << endl;
    cout << "top 10 : " << result.top10 << " / " << result.total << " = " << (float)result.top10 / (float)result.total << endl;
    cout << "top 50 : " << result.top50 << " / " << result.total << " = " << (float)result.top50 / (float)result.total << endl;
}

void StRecognition::testOneBysShu(vector<float> feature, int topk, SearchResult &result)
{
	int nK = topk * mFactor;//mFactor 默认是1
	vector<long> idxs;
	vector<float> dist;
	mIndex->search(feature, nK, idxs, dist);//第一次

	if (idxs.size() == 0)
		return;

	vector<ScoreNode> vtNodes;
	for (int i = 0; i < nK; i++) {
		if (idxs[i] <= 0)
			continue;
		string strName;
		FeatureValue ftValue;
		if (!MongoDBPtr->search(idxs[i], strName, ftValue))//第二次
		{
			cout << "search feature datebase failed!" << endl;
			continue;
		}
		float realDist = computerDistance(feature, ftValue.data());
		vtNodes.push_back(ScoreNode(idxs[i], strName, dist[i], realDist));
	}

	std::sort(vtNodes.begin(), vtNodes.end(), greater<ScoreNode>());
	for (size_t i = 0; i < vtNodes.size(); i++) {
		if (i >= topk)
			break;
		showResult(i + 1, vtNodes[i].name, vtNodes[i].dist,
				vtNodes[i].realDist);
		/*if (isSame(image, vtNodes[i].name)) {
			if (i == 0)
				result.top1++;
			if (i < 10)
				result.top10++;
			if (i < 50)
				result.top50++;
		}*/
	}
}


/***将栋梁传过来的特征向量转化为子向量，其中start表示取的第一个位置，end表示相应的最后一个位置，位置从0开始***/

vector<float> StRecognition::getIndexFeature(vector<float> features, size_t start, size_t end)
{
	if(start>features.size() || end >features.size())
	{
		cout<<"out of range.......!!";
		return vector<float>();
	}
	else{
		return vector<float>(features.begin()+start,features.begin()+end);
	}
}

/**************************************************
 * @参数:*******************************************
 * features:传入的每一个人脸的特征向量******************
 * topK:对于每一个特征最匹配的前K数量*******************
 * vtNodes:输出参数，排好序的特征信息的ScoreNode集合*****
 * 该函数主要功能是：将传入的特征与数据库中信息做比对，并****
 * 将比对信息存储与vtNodes里面************************
 **************************************************/

void StRecognition::search( FeatureValue features, int topK, vector<ScoreNode> vtNodes)
{
	int nK = mType == IVFPQ ? topK * mFactor : topK;
	vector<long> idxs;
	vector<float> dist;
	mIndex->search(features, nK, idxs, dist);
	if (idxs.size() == 0)
		return;
	for (int i = 0; i < nK; i++) {
		if (idxs[i] <= 0)
			continue;
		string strName;
		FeatureValue ftValue;
		if (!MongoDBPtr->search(idxs[i], strName, ftValue))
		{
			cout << "search feature datebase failed!" << endl;
			continue;
		}
		float realDist = computerDistance(features, ftValue.data());
		if (mType == FlatIP)
			showResult(i + 1, strName, dist[i], realDist);
		else {
			vtNodes.push_back(ScoreNode(idxs[i], strName, dist[i], realDist));
		}
	}
	if (mType == IVFPQ)
		std::sort(vtNodes.begin(), vtNodes.end(), greater<ScoreNode>());
}

/**************************************************
 * @参数:*******************************************
 * features:传入的特征向量集合（每一个特征占据256维）*****
 * 该函数主要是将：1,传入的特征向量集合拆分成每一个256维****
 * 的子向量，     2,将每一个特征都传入数据库中去做搜索，****
 * 并将搜索的结果存储在向量中。*************************
 **************************************************/

void StRecognition::searchTest(vector<float> features)
{
	if( features.size() == (size_t)0 )
	{
		cout<<" the number of the features is 0.......!";
		return;
	}
	size_t numOfFeatures = features.size()/256;
	vector< vector<ScoreNode> >vtNodes(numOfFeatures,vector<ScoreNode>());
	for(size_t i=0; i<numOfFeatures; ++i)
	{
		vector<float> feature = getIndexFeature(features, i*256, (i+1)*256-1);
		search(feature, 10, vtNodes[i]);
	}

}
