/**************************************************************************************************
 * @file   : StGpuIndex
 * @brief  : 特征搜索，使用faiss库建索引
 * @date   : 2017-05-22
 * @remarks:
 **************************************************************************************************/

#include <glog/logging.h>
#include "GpuIndexIVFPQ.h"
#include "GpuIndexFlat.h"
#include "index_io.h"
#include "GpuAutoTune.h"
#include "StandardGpuResources.h"

#include "StGpuIndex.h"

using namespace std;

StGpuIndex::StGpuIndex(IndexType type) : mType(type), mIndex(NULL), mCurID(0)
{
    mResources = new faiss::gpu::StandardGpuResources;
}

StGpuIndex::~StGpuIndex()
{
    if(mIndex)
    {
        delete mIndex;
    }
    delete mResources;
}

//加载模型
bool StGpuIndex::open(int deviceID, const std::string &indexFile)
{
    if(mIndex){
        LOG(ERROR) << "index file is opened" << endl;
        return false;
    }

    if(mType == IVFPQ)
    {
        faiss::Index * cpuIndex = faiss::read_index(indexFile.c_str());
        faiss::gpu::GpuClonerOptions options;
        options.indicesOptions = faiss::gpu::INDICES_32_BIT;
        options.usePrecomputed = false;
        options.reserveVecs = 1024 * 1024 * 20;
        mIndex = dynamic_cast<faiss::gpu::GpuIndexIVFPQ *>(faiss::gpu::index_cpu_to_gpu(mResources, deviceID, cpuIndex, &options));

        delete cpuIndex;
    }
    else
    {
        faiss::Index * cpuIndex = faiss::read_index(indexFile.c_str());
        mIndex = dynamic_cast<faiss::gpu::GpuIndex *>(faiss::gpu::index_cpu_to_gpu(mResources, deviceID, cpuIndex));
        delete cpuIndex;
    }

    if(mIndex == NULL)
    {
        LOG(ERROR) << "load index file failed!" << endl;
        return false;
    }

    mCurID = mIndex->ntotal;
    return true;
}

bool StGpuIndex::open(int deviceID, int featureDim)
{
    if(mIndex)
    {
        LOG(ERROR) << "index file is opened" << endl;
        return false;
    }
    if(mType == IVFPQ)
    {
        LOG(ERROR) << "error:use IVFPQ index must have index file!" << endl;
        return false;
    }
    if(mType == FlatIP)
    {
        faiss::gpu::GpuIndexFlatConfig config;
        config.device = deviceID;
        mIndex = new faiss::gpu::GpuIndexFlatIP(mResources, featureDim, config);
    }
    mCurID = 0;
    return true;
}

//特征数据建立索引
void StGpuIndex::addToIndex(int count, const std::vector<float> &featureData, std::vector<long> &ids)
{
    ids.resize(count);
    for(int i = 0; i < count; i++)
    {
        ids[i] = mCurID++;
    }
    switch(mType)
    {
    case FlatIP:
        mIndex->add(count, featureData.data());
        break;
    case IVFPQ:
        mIndex->add_with_ids(count, featureData.data(), ids.data());
        break;
    default:
        break;
    }
}

void StGpuIndex::addToIndex(int count, cv_faceFeature_t *features, std::vector<long> &ids)
{
    int nDim = features[0].feature_len;
    vector<float> vtFeatures(count * nDim);
    float * buf = vtFeatures.data();
    ids.resize(count);
    for(int i = 0; i < count; i++)
    {
        memcpy(buf + i * nDim, &features[i].feature[0], sizeof(float) * nDim);
        ids[i] = mCurID++;
    }
    if(mType == FlatIP)
        mIndex->add(count, vtFeatures.data());
    else
        mIndex->add_with_ids(count, vtFeatures.data(), ids.data());
}

//保存索引文件
bool StGpuIndex::saveIndex(const std::string &fileName)
{
    faiss::Index *cpuIndex = faiss::gpu::index_gpu_to_cpu(mIndex);
    faiss::write_index(cpuIndex, fileName.c_str());

    delete cpuIndex;
    return true;
}

//当前索引的总数
int StGpuIndex::indexCount()
{
    if(mIndex)
        return mIndex->ntotal;
    else
        return -1;
}

//设置探测的区域数
void StGpuIndex::setProbes(int probe)
{
    if(mType != IVFPQ)
        return;
    if(probe < 1)
        return;
    if(probe > 1024)
        probe = 1024;
    faiss::gpu::GpuIndexIVFPQ * ivfIndex = dynamic_cast<faiss::gpu::GpuIndexIVFPQ *>(mIndex);
    if(!ivfIndex)
        return;
    ivfIndex->setNumProbes(probe);
}

//搜索
bool StGpuIndex::search(const FeatureValue &feature, int topK, std::vector<long> &idxs, std::vector<float> &distance)
{
    if(feature.size() == 0)
        return false;
    if(topK <= 0 || topK > 1024)
    {
        LOG(ERROR) << "search top " << topK << " is not support!" << endl;
        return false;
    }

    distance.resize(topK);
    idxs.resize(topK);

    TS(t);
    mIndex->search(1, feature.data(), topK, distance.data(), idxs.data());//在这里变成了１０００
   ／／ TE(t, "search time: ");
    return true;
}

bool StGpuIndex::search(const std::vector<FeatureValue> &features, int topK, std::vector<std::vector<long>> &idxs, std::vector<std::vector<float>> &distance)
{
        if(topK <= 0 || topK > 1024)
        {
            LOG(ERROR) << "search top " << topK << " is not support!" << endl;
            return false;
        }
        int dim = features[0].size();
        int count = features.size();
        vector<float> vtQuery(dim * count);
        for(int i = 0; i < count; i++)
        {
            if(features[i].size() != dim)
            {
                LOG(ERROR) << "features dimension is different!" << endl;
                return false;
            }
            memcpy(vtQuery.data() + i * dim, features[i].data(), sizeof(float) * dim);
        }
        vector<float> vtDist(topK * count);
        vector<long> vtIdx(topK * count);

        mIndex->search(count, vtQuery.data(), topK, vtDist.data(), vtIdx.data());

        idxs.resize(count);
        distance.resize(count);
        for(int i = 0; i < count; i++)
        {
            idxs[i].resize(topK);
            distance[i].resize(topK);
            memcpy(idxs[i].data(), vtIdx.data() + i * topK, sizeof(faiss::Index::idx_t) * topK);
            memcpy(distance[i].data(), vtDist.data() + i * topK, sizeof(float) * topK);
        }
        return true;
}




