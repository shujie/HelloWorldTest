/**************************************************************************************************
 * @file   : StInDB.cpp
 * @brief  : 入库示例
 * @date   : 2017-05-08
 * @remarks:
 **************************************************************************************************/
#include <fstream>
#include <glog/logging.h>
#include "StDistributor.h"
#include "StInDB.h"
#include "StMongoDB.h"
#include "StGpuIndex.h"

using namespace std;
void readFeatureFromText(
		const std::string targetFileName, std::vector<float>& feature_target,
		const std::string sourceFileName, std::vector<float>& feature_source,
		std::vector<size_t>& srcIdx, int dim)
{
	/// read target file
	std::vector<size_t> idx_target;
	readIdxAndFeatureFromText(targetFileName, idx_target, feature_target, dim);

	/// read source file
	std::vector<size_t> idx_source;
	readIdxAndFeatureFromText(sourceFileName, idx_source, feature_source, dim);

	LOG(INFO) << "idx_source.size: " << idx_source.size();
	/// match source and target
	for(size_t i = 0; i < idx_source.size(); i++)
	{
		/// search for index
		std::vector<size_t>::iterator it = std::find(idx_target.begin(), idx_target.end(), idx_source[i]);
		if (it == idx_target.end())
		{
			LOG(FATAL) << "source index not find in target indexMap";
		}
		srcIdx.push_back(std::distance(idx_target.begin(), it));
	}
//
//	{// DEBUG
//		std::cout
//			<< "FUCK1"
//			<< ", srcIdx: " << srcIdx.size()
//			<< ", dim: " << dim
//			<<  std::endl;
//		int feature_source_size = srcIdx.size();
//    	for (int i = 0; i < feature_source_size; i++)
//    	{
//    		std::string features;
//    		for (int j = 0; j < dim; j++)
//    		{
//    			features += std::to_string(feature_source[i * dim + j]);
//    			features += ", ";
//    		}
//    		std::cout
//    			<< "Before search"
//    			<< ", index: " << i
//				<< ", feature: " << features
//				<< std::endl;
//    	}
//		std::cout << "FUCK2" << std::endl;
//	}
//
//	{// DEBUG
//		std::string features;
//		for (int i = 0; i < dim*4; i++)
//		{
//			features.push_back(((char*)feature_source.data())[i]);
//		}
//
//		std::cout
//			<< "Before search"
//			<< ", features: " << features
//			<< std::endl;
//	}
}

/**将特征从文件中读取出来，并存取成向量的形式**/

void getFeaturesAndIndexs(std::vector<float>& target_features, std::vector<float>& source_features,
								std::vector<float>& disturb_features, std::vector<size_t> indexMap)
{
	readFeatureFromText("feature_target.txt", target_features, "feature_source.txt", source_features, indexMap, 256);
	readFeatureFromFile("1kw/1kw.feature", disturb_features, FEATURE_LEN);
}


StInDB::StInDB() : mDistributor(NULL), mConf(NULL), mFeatureCount(0), mSimCount(0)
{
    mStatus = new StatusInfo;
}

StInDB::~StInDB()
{
    if(mDistributor)
        delete mDistributor;

    delete mStatus;
}

//初始化索引
bool StInDB::initIndex(int deviceID, const IndexConfig &info)
{
    mIndexConfig = info;
    bool ret = true;
    mIndex = new StGpuIndex(info.type);
    if(info.type == FlatIP)
    {
        ret = mIndex->open(deviceID, info.dim);
    }
    else
    {
        ret = mIndex->open(deviceID, info.srcFile);//？？？？？？

    }
    return ret;
}

//设置特征提取的参数
void StInDB::setExtractArg(const std::vector<std::string> &dataDirs, ExtractConf *extConf, int featureDim)
{
    mConf = extConf;
    mFeatureDim = featureDim;
    mDistributor = new StFileDistributor(extConf, dataDirs);
    mDistributor->setDBInterface(this);
    mDistributor->setMonitor(this);
}

//重写的特征数据接口
void StInDB::outFeatures(VerifyInfo *vfInfo, cv_faceFeature_t *features)
{
    //建立索引
    vector<long> ids;
    mIndex->addToIndex(vfInfo->mats.size(), features, ids);
    //插入到数据库
    MongoDBPtr->insertBatch(ids.data(), vfInfo->names, features);
}

//事件处理
void StInDB::event(QmEvent *aEvent)
{
    switch(aEvent->type())
    {
    case QmEvent::ET_User + 1 :
        {
            StDetectEvent *pDetectEvent = dynamic_cast<StDetectEvent *>(aEvent);
            vector<StatusUnit> vtStatus;
            pDetectEvent->getStatus(vtStatus);
            for(size_t i = 0; i< vtStatus.size(); i++)
            {
                statisticStatus(vtStatus[i]);
            }
        }
        break;
    case QmEvent::ET_User + 2 :
        {
            StExtractEvent *pExtEvent = dynamic_cast<StExtractEvent *>(aEvent);
            switch(pExtEvent->status())
            {
            case SC_VerifyError:
                mStatus->verifyError += pExtEvent->imageCount();
                break;
            case SC_CompleteExtract:
                mStatus->imageCount += pExtEvent->imageCount();
                break;
            default:
                break;
            }
        }
        break;
    default:
        break;
    }
    showStatus();
}

//开始训练
void StInDB::start()
{
    if(!mDistributor)
    {
        LOG(ERROR) << "feature extract arg not set!" << endl;
        return;
    }

    //开启分发
    TS(ext);
    mDistributor->start();
    eventLoop();
    printf("\r\n");

    //模拟数据
    simulate();
    printf("\r\n");
    mIndex->saveIndex(mIndexConfig.dstFile);
    TE(ext, "extract time: ");
}

//统计状态
void StInDB::statisticStatus(const StatusUnit &unit)
{
    switch(unit.status)
    {
    case SC_ReadImageError:
        mStatus->readImageError++;
        break;
    case SC_DetectError:
        mStatus->detectError++;
        break;
    case SC_NoFace:
        mStatus->noFace++;
        break;
    case SC_AlignError:
        mStatus->alignError++;
        break;
    case SC_BlackAndWhite:
        mStatus->filterBlackAndWhite++;
        break;
    case SC_QualityLimit:
        mStatus->qualityLimit++;
        break;
    case SC_FaceSizeLimit:
        mStatus->faceSizeLimit++;
        break;
    case SC_VerifyError:
        mStatus->verifyError++;
        break;
    default:
        break;
    }
}

//显示进度状态
void StInDB::showStatus()
{
    printf("Success image:%d, Read Error:%d, Detect Error:%d, NoFace:%d, AlignError:%d, FilterBlackAndWhite:%d, QualityLimit:%d, FaceSizeLimit:%d, VerifyError:%d\r",
           mStatus->imageCount, mStatus->readImageError, mStatus->detectError, mStatus->noFace, mStatus->alignError, mStatus->filterBlackAndWhite,
           mStatus->qualityLimit, mStatus->faceSizeLimit, mStatus->verifyError);
    fflush(stdout);
}

//设置模拟源数据文件
void StInDB::setDataFile(const std::string &dataFile, int simCount)
{
    mDataFile = dataFile;
    mSimCount = simCount;
}

void StInDB::simulate()
{
    if(mSimCount <= 0)
        return;
    ifstream in(mDataFile.c_str(), std::ios::binary);
    for(int i = 0; i < mSimCount; i++)
    {
        simulateOne(in);
    }
    in.close();
}

void StInDB::startTFeatures()
{
    std::vector<size_t> indexMap;
    std::vector<float> source_features;
    std::vector<float> target_features;

	readFeatureFromText("feature_target.txt", target_features, "feature_source.txt", source_features, indexMap, 256);

}

void StInDB::simulateOne(std::ifstream &in)
{
    int ftCount = 200000;
    int batch = 100;
    int m = 0;
    vector<float> vtData(batch * mIndexConfig.dim);
    vector<long> ids;
    vector<string> vtNames(batch);
    for(int i = 0; i < ftCount; i++)
    {
        float * buf = vtData.data() + m * mFeatureDim;
        in.read((char *)buf, sizeof(float) * mIndexConfig.dim);
        forgeFeature(buf, mFeatureDim);
        vtNames[m] = "simulate datas";
        if((m + 1) % batch == 0)
        {
            mIndex->addToIndex(batch, vtData, ids);
            MongoDBPtr->insertBatch(ids.data(), vtNames, vtData, mIndexConfig.dim);
            mStatus->imageCount += batch;
            printf("simulate features : %d\r", mStatus->imageCount);
            fflush(stdout);
            m = 0;
        }
        else
            m++;
    }
    in.seekg(0);
}

//DB中插入真实的数据
void StInDB::insertTFeatures(vector<float>& features)
{
	size_t ftCount = features.size();
	int batch = 100;
	int m = 0;
	vector<float> vtData(batch * 256);
	vector<long> ids;
	vector<string> vtNames(batch);

	for (size_t i = 0; i < ftCount; i++) {
		float * buf = vtData.data() + m * 256;
		vtNames[m] = "true datas";
		if ((m + 1) % batch == 0) {
			mIndex->addToIndex(batch, vtData, ids);
			MongoDBPtr->insertBatch(ids.data(), vtNames, vtData, 256);
			mStatus->imageCount += batch;
			printf("true features : %d\r", mStatus->imageCount);
			fflush (stdout);
			m = 0;
		} else
			m++;
	}

}
//伪造特征数据
void StInDB::forgeFeature(float *ft, int len)
{
    double dSum = 0.0;
    for(int i = 0; i < len; i++)
    {
        ft[i] = drand48();//(float)(rand() % 100) / 100.0;
        dSum += ft[i] * ft[i];
    }
    dSum = sqrt(dSum);
    for(int i = 0; i < len; i++)
    {
        ft[i] = ft[i] / dSum;
    }
}
