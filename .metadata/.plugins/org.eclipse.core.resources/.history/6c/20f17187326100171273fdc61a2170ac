/**************************************************************************************************
 * @file   : StIndexTrain.h
 * @brief  : 索引训练
 * @date   : 2017-05-08
 * @remarks:
 **************************************************************************************************/

#ifndef STINDEXTRAIN_H
#define STINDEXTRAIN_H
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <sys/time.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <string>
#include <opencv2/opencv.hpp>
#include <random>
#include <iterator>
#include "glog/logging.h"
#include <string>
#include "SACoreDef.h"
#include "StEvent.h"
using namespace std;


class StFileDistributor;

class SACORE_API StIndexTrain : public QmNotify, public IOutFeature
{
public:
    StIndexTrain();
    virtual ~StIndexTrain();

    //设置特征提取的参数
    void setExtractArg(const std::string &dataDir, ExtractConf *extConf, int featureDim = 128);
    //重写的特征数据接口
    virtual void outFeatures(VerifyInfo *vfInfo, cv_faceFeature_t *features);
    //事件处理
    virtual void event(QmEvent *aEvent);
    //开始训练
    void startTrain(const std::string &fileName, int subQuantizers = 8);
    //训练数据
    void train(const std::string &fileName, int subQuantizers);

    //提取出的特征总数
     int mFeatureCount;
     void StIndexTrain::readIdxAndFeatureFromText(const std::string fileName, std::vector<size_t>& idxs, std::vector<float>& features, size_t dim);
     void readFeatureFromText(
     		const std::string targetFileName, std::vector<float>& feature_target,
     		const std::string sourceFileName, std::vector<float>& feature_source,
     		std::vector<size_t>& srcIdx, int dim);
     void readFeatureFromFile(const std::string fileName, std::vector<float>& feature, int dim);
     void DecodeBase64(const char* Data, int DataByte, int& OutByte, std::string& strDecode);

private:

    //统计状态
    void statisticStatus(const StatusUnit &unit);
    //显示进度状态
    void showStatus();
private:
    StFileDistributor *mDistributor;

    //输出的临时特征记录文件
    std::ofstream mOut;
    //状态信息
    StatusInfo *mStatus;
    //特征维度
    int mFeatureDim;
    ExtractConf *mConf;
};

#endif // STINDEXTRAIN

