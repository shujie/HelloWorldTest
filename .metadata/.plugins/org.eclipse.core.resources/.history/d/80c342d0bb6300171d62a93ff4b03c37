/**************************************************************************************************
 * @file   : StIndexTrain.cpp
 * @brief  : 索引训练
 * @date   : 2017-05-08
 * @remarks:
 **************************************************************************************************/
#include <glog/logging.h>
#include "StDistributor.h"
#include "StIndexTrain.h"

#include "GpuIndexIVFPQ.h"
#include "index_io.h"
#include "GpuAutoTune.h"
#include "StandardGpuResources.h"
#include "GpuIndexFlat.h"


const static int PADDING = 16 + 12;
const static int FEATURE_LEN = 256;

/// db initialization
const static int DEVICE_ID = 0;
const static size_t DIM = FEATURE_LEN;
const static size_t N_CENTROIDS = 1882;
const static int N_SUB_QUANTIZIERS = 8;
const static int BIT_PER_CODE = 8;
const static size_t N_PROBES = 60;
const static size_t N_TRAIN_SET = 25 * 10000;


using namespace std;



StIndexTrain::StIndexTrain() : mDistributor(NULL), mConf(NULL), mFeatureCount(0)
{
    mStatus = new StatusInfo;
}

StIndexTrain::~StIndexTrain()
{
    if(mDistributor)
        delete mDistributor;

    delete mStatus;
}

//设置特征提取的参数
void StIndexTrain::setExtractArg(const std::string &dataDir, ExtractConf *extConf, int featureDim)
{
    mConf = extConf;
    mFeatureDim = featureDim;
    vector<string> vtDir;
    cout<<dataDir<<endl;
    vtDir.push_back(dataDir);
    mDistributor = new StFileDistributor(extConf, vtDir, true);
    mDistributor->setDBInterface(this);
    mDistributor->setMonitor(this);
}

//重写的特征数据接口
void StIndexTrain::outFeatures(VerifyInfo *vfInfo, cv_faceFeature_t *features)
{
    for(size_t i = 0; i < vfInfo->mats.size(); i++)
    {
        mOut.write((char *)(features[i].feature), sizeof(float) * features[i].feature_len);
    }
    mFeatureCount += vfInfo->mats.size();
}

//事件处理
void StIndexTrain::event(QmEvent *aEvent)
{
    switch(aEvent->type())
    {
    case QmEvent::ET_User + 1 :
        {
            StDetectEvent *pDetectEvent = dynamic_cast<StDetectEvent *>(aEvent);
            vector<StatusUnit> vtStatus;
            pDetectEvent->getStatus(vtStatus);
            for(size_t i = 0; i< vtStatus.size(); i++)
            {
                statisticStatus(vtStatus[i]);
            }
        }
        break;
    case QmEvent::ET_User + 2 :
        {
            StExtractEvent *pExtEvent = dynamic_cast<StExtractEvent *>(aEvent);
            switch(pExtEvent->status())
            {
            case SC_VerifyError:
                mStatus->verifyError += pExtEvent->imageCount();
                break;
            case SC_CompleteExtract:
                mStatus->imageCount += pExtEvent->imageCount();
                break;
            default:
                break;
            }
        }
        break;
    default:
        break;
    }
    showStatus();
}

//开始训练
void StIndexTrain::startTrain(const std::string &fileName, int subQuantizers)
{
    if(!mDistributor)
    {
        LOG(ERROR) << "feature extract arg not set!" << endl;
        return;
    }
    mOut.open("./featureData", std::ios::binary);
    //开启分发
    TS(ext);
    mDistributor->start();
    eventLoop();
    printf("\r\n");
    TE(ext, "extract time: ");
    mOut.close();
    //开始训练数据
    train(fileName, subQuantizers);
}

//统计状态
void StIndexTrain::statisticStatus(const StatusUnit &unit)
{
    switch(unit.status)
    {
    case SC_ReadImageError:
        mStatus->readImageError++;
        break;
    case SC_DetectError:
        mStatus->detectError++;
        break;
    case SC_NoFace:
        mStatus->noFace++;
        break;
    case SC_AlignError:
        mStatus->alignError++;
        break;
    case SC_BlackAndWhite:
        mStatus->filterBlackAndWhite++;
        break;
    case SC_QualityLimit:
        mStatus->qualityLimit++;
        break;
    case SC_FaceSizeLimit:
        mStatus->faceSizeLimit++;
        break;
    case SC_VerifyError:
        mStatus->verifyError++;
        break;
    default:
        break;
    }
}

//显示进度状态
void StIndexTrain::showStatus()
{
    printf("Success image:%d, Read Error:%d, Detect Error:%d, NoFace:%d, AlignError:%d, FilterBlackAndWhite:%d, QualityLimit:%d, FaceSizeLimit:%d, VerifyError:%d\r",
           mStatus->imageCount, mStatus->readImageError, mStatus->detectError, mStatus->noFace, mStatus->alignError, mStatus->filterBlackAndWhite,
           mStatus->qualityLimit, mStatus->faceSizeLimit, mStatus->verifyError);
    fflush(stdout);
}

void StIndexTrain::DecodeBase64(const char* Data, int DataByte, int& OutByte, std::string& strDecode)
{
    const char DecodeTable[] = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        62, // '+'
        0, 0, 0,
        63, // '/'
        52, 53, 54, 55, 56, 57, 58, 59, 60, 61, // '0'-'9'
        0, 0, 0, 0, 0, 0, 0,
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
        13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, // 'A'-'Z'
        0, 0, 0, 0, 0, 0,
        26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
        39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, // 'a'-'z'
    };

    int nValue;
    int i= 0;
    while (i < DataByte) {
        if (*Data != '\r' && *Data!='\n') {
            nValue = DecodeTable[*Data++] << 18;
            nValue += DecodeTable[*Data++] << 12;
            strDecode+=(nValue & 0x00FF0000) >> 16;
            OutByte++;
            if (*Data != '=') {
                nValue += DecodeTable[*Data++] << 6;
                strDecode+=(nValue & 0x0000FF00) >> 8;
                OutByte++;
                if (*Data != '=') {
                    nValue += DecodeTable[*Data++];
                    strDecode+=nValue & 0x000000FF;
                    OutByte++;
                }
            }
            i += 4;
        }
        else {
            Data++;
            i++;
        }
    }
}


void StIndexTrain::readFeatureFromFile(const std::string fileName, std::vector<float>& feature, int dim)
{
	/// probe file size and data base size
	std::ifstream ifs(fileName, std::ifstream::binary);
	ifs.seekg(0, ifs.end);
	size_t fileSize =  ifs.tellg();
	size_t dbSize = fileSize / (PADDING + dim * sizeof(float));
	//dbSize = 1000;//warning: !!!!!!注意改过来
	feature.resize(dbSize * dim, 0);
	ifs.seekg(0, ifs.beg);

	LOG(INFO)
			<< "readFeatureFromFile: " << fileName
			<< ", fileSize: " << fileSize
			<< ", dbSize: " << dbSize
			<< ", dim: " << dim;

	/// read data into vector
	for (size_t i = 0; i < dbSize; i++)
	{
		ifs.ignore(16 + 12);
		ifs.read((char*)(feature.data() + i * dim), dim * sizeof(float));

	}
	ifs.close();
}

void StIndexTrain::readIdxAndFeatureFromText(const std::string fileName, std::vector<size_t>& idxs, std::vector<float>& features, size_t dim)
{
	std::ifstream ifs(fileName);
	std::string line;
	std::vector<float> featureTemp;
	featureTemp.resize(dim, 0);
	while(true)
	{
		/// extract index from file name
		std::getline(ifs, line);
		if (ifs.eof())
			break;
		size_t idx;
		size_t first_pos = line.find('_');
		size_t second_pos = line.find('_', first_pos + 1);
		line = line.substr(first_pos + 1, second_pos - first_pos - 1);
		std::stringstream ss(line);
		ss >> idx;
		idxs.push_back(idx);

		/// extract feature
		std::getline(ifs, line);
		std::string decodeStr;
		int decodeLen = 0;
		DecodeBase64(line.c_str(), line.length(), decodeLen, decodeStr);

		decodeStr = decodeStr.substr(decodeStr.length() - dim * sizeof(float),  std::string::npos);
		memcpy((void*)featureTemp.data(), (void*)decodeStr.data(), dim * sizeof(float));
		features.insert(features.end(), featureTemp.begin(), featureTemp.end());
	}
	ifs.close();
}



void StIndexTrain::readFeatureFromText(const std::string targetFileName,
		std::vector<float>& feature_target, const std::string sourceFileName,
		std::vector<float>& feature_source, std::vector<size_t>& srcIdx,
		int dim) {
	/// read target file
	std::vector < size_t > idx_target;
	readIdxAndFeatureFromText(targetFileName, idx_target, feature_target, dim);

	/// read source file
	std::vector < size_t > idx_source;
	readIdxAndFeatureFromText(sourceFileName, idx_source, feature_source, dim);

	LOG(INFO) << "idx_source.size: " << idx_source.size();
	/// match source and target
	for (size_t i = 0; i < idx_source.size(); i++) {
		/// search for index
		std::vector<size_t>::iterator it = std::find(idx_target.begin(),
				idx_target.end(), idx_source[i]);
		if (it == idx_target.end()) {
			LOG(FATAL) << "source index not find in target indexMap";
		}
		srcIdx.push_back(std::distance(idx_target.begin(), it));
	}
}

//训练数据
void StIndexTrain::train(const std::string &fileName, int subQuantizers)
{
//	std::vector<size_t>& idxs;
//	std::vector<float>& features;
//    uint count = 25 * 10000;
//    vector<float> vtData(count);
//    float *buf = &vtData[0];
//    for(int i = 0; i < count; i++)
//    {
//        in.read((char *)(buf + i * mFeatureDim), sizeof(float) * mFeatureDim);
//    }
//    in.close();
	std::vector<float> features;
	cout<<"satrt! load feature file!!!!!!"<<endl;
	readFeatureFromFile("./1kw/1kw.feature", features, 256);
	cout<<"load feature file sucess"<<endl;
    faiss::gpu::StandardGpuResources resources;

    int ncentroids = int (4 * sqrt (mFeatureCount));
    //int ncentroids = 100;
//    if(ncentroids > 1024)
//        ncentroids = 1024;
    cout<<"start gpu index IVFPQ"<<endl;
    faiss::gpu::GpuIndexIVFPQ gpuIndex(&resources, 0, 256,
                                      1882, 8, 8, true,
                                      faiss::gpu::INDICES_64_BIT,
                                      false,
                                      faiss::METRIC_L2);
    cout<<"gpu index IVFPQ sucess"<<endl;
    //faiss::METRIC_INNER_PRODUCT还没有实现
    gpuIndex.verbose = true;
    cout<<"train start"<<endl;
    gpuIndex.train( 25 * 10000, features.data());
    std::cout<<"getCentroidsPerSubQuantizer: "<<gpuIndex.getCentroidsPerSubQuantizer()<<" getNumSubQuantizers: "<<gpuIndex.getNumSubQuantizers()<<std::endl;
    cout<<"train sucess"<<endl;
    faiss::Index *cpuIndex = faiss::gpu::index_gpu_to_cpu(&gpuIndex);
    faiss::write_index(cpuIndex, fileName.c_str());
    std::cout<<"cpuIndex->ntotal: "<<cpuIndex->ntotal<<"cpuIndex->d: "<<cpuIndex->d<<std::endl;
    delete cpuIndex;
}
